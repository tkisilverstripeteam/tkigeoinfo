<?php
/**
 */
class TkiGeoInfo {
	
	public static $add_select_option = true;
	public static $add_other_subdivision = true;
	
	protected static $countries = array();		// Cached
	protected static $admin_areas_1 = array();	// Cached
	
	protected static $iso_3166_subdivisions = array(
		'AU' => array(
			'AU-NSW' => 'New South Wales',
			'AU-QLD' => 'Queensland',
			'AU-SA' => 'South Australia',
			'AU-TAS' => 'Tasmania',
			'AU-VIC' => 'Victoria',
			'AU-WA' => 'Western Australia',
			'AU-ACT' => 'Australian Capital Territory',
			'AU-NT' => 'Northern Territory'
		),
		'NZ' => array(
			'NZ-NTL' => 'Northland',
			'NZ-AUK' => 'Auckland Region',
			'NZ-WKO' => 'Waikato',
			'NZ-BOP' => 'Bay of Plenty',
			'NZ-GIS' => 'Gisborne District',
			'NZ-HKB' => "Hawke's Bay",
			'NZ-TKI' => 'Taranaki',
			'NZ-MWT' => 'Manawatu-Wanganui',
			'NZ-WGN' => 'Wellington',
			'NZ-TAS' => 'Tasman District',
			'NZ-NSN' => 'Nelson City',
			'NZ-MBH' => 'Marlborough District',
			'NZ-WTC' => 'West Coast',
			'NZ-CAN' => 'Canterbury',
			'NZ-OTA' => 'Otago',
			'NZ-STL' => 'Southland',
			'NZ-CIT' => 'Chatham Islands Territory'
		)
	);
	
	public static function get_subdivisions($countries=null) {
		$out = array();
			// Subdivisions for a specific country
		if(!empty($countries)) {
			$countries = (array) $countries;
			foreach($countries as $code) {
				if(array_key_exists($code,self::$iso_3166_subdivisions)
				&& is_array(self::$iso_3166_subdivisions[$code]) 
				&& !empty(self::$iso_3166_subdivisions[$code])) {
					$out = array_merge($out,self::$iso_3166_subdivisions[$code]);
				}
			}
			
		} else {
				// All subdivisions
			foreach(self::$iso_3166_subdivisions as $k => $v) {
				$out = array_merge($out,$v);
			}
		}
		return $out;
	}
	
	/** 
	 * Returns the subdvision name from the appropriate code.
	 * @return null|string String if subdvision found, null if none found
	 */
	public static function find_subdivision_name($code,$country=null) {
		$subdivisions = self::get_subdivisions($country);
		$name = $code;
		if(!empty($code) && array_key_exists($code,$subdivisions) 
			&& !empty($subdivisions[$code])) {
			$name = $subdivisions[$code];
		}
		if($code === 'OTHER') $name = '';
		return $name;
	}
	
	public static function get_subdivision_dropdown($countries=null) {
		$out = array();
		$subdivisions = self::get_subdivisions($countries);
		if(is_array($subdivisions) && !empty($subdivisions)) {
			if(self::$add_select_option && count($subdivisions) > 1) {
				$out[''] = _t('TkiGeoInfo.SUBDIVISIONSELECT','Select');
			}
			$out = array_merge($out,$subdivisions);
		}
		if(self::$add_other_subdivision) {
			$out['OTHER'] = _t('TkiGeoInfo.OTHER','Other');
		}
		return $out;
	}
	
	public static function is_valid_subdivision($code,$country=null) {
		$subdivisions = self::get_subdivisions($country);
		
		if(self::$add_other_subdivision) {
			$subdivisions['OTHER'] = _t('TkiGeoInfo.OTHER','Other');
		}
		if($code && array_key_exists($code,$subdivisions)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static function get_countries() {
		if(!empty(self::$countries)) return self::$countries;	// Cached
		$countries = DataObject::get('TkiGeoCountry','','Title ASC');
		if($countries && $countries->exists()) {
			self::$countries = $countries->map('Code');
		}
		return self::$countries;
	}
	
	public static function find_country_name($code) {
		$countries = self::get_countries();
		$name = $code;
		if(!empty($code) && array_key_exists($code,$countries) 
			&& !empty($countries[$code])) {
			$name = $countries[$code];
		}
		return $name;
	}
	
	public static function get_country_dropdown($codes=array()) {
		$out = array();
		$countries = self::get_countries();
		$options = array();
			// Limited country list - codes supplied as argument
		if(!empty($codes)) foreach($codes as $code) {
				$options[$code] = $countries[$code];
		} else {
			$options = $countries;	// Full country list
		}
		if(is_array($options) && !empty($options)) {
			if(self::$add_select_option && count($options) > 1) {
				$out[''] = _t('TkiGeoInfo.COUNTRYSELECT','Select');
			}
			$out = array_merge($out,$countries);
		}
		return $out;
	}
	
	public static function is_valid_country($code) {
		$countries = Geoip::getCountryDropDown();
		return array_key_exists($code,$countries);
	}
	
	/* ------------ Database methods ------------ */
		// Base method
	public function get_area($filter,$class=null,$multiple=false) {
		$class = (!empty($class)) ? $class : 'TkiGeoArea';
		$res = ($multiple) ? DataObject::get($class,$filter) : DataObject::get_one($class,$filter);
		return ($res && $res->exists()) ? $res : null;
	}
	
	public static function get_area_by_code($arg,$class=null) {
		if(empty($arg)) return null;
		$filter = "\"Code\"='". Convert::raw2sql($arg) ."'";
		return self::get_area($filter,$class);
	}
	
	public static function get_country_by_title($arg) {
		if(empty($arg)) return null;
		$filter = "\"Title\"='". Convert::raw2sql($arg) ."'";
		return self::get_area($filter,'TkiGeoCountry');
	}
	
	public static function get_country_id($arg) {
		$res = self::get_country($arg);
		return ($res && $res->exists()) ? (int) $res->ID : null;
	}
	
	public static function get_country($arg) {
		if(empty($arg)) return null;
		if(strlen($arg) === 2) {
			$field = 'Code';
			$filter = "\"Code\"='". Convert::raw2sql($arg) ."'";
		} else {
			$field = 'Title';
			$filter = "\"Title\"='". Convert::raw2sql($arg) ."'";
			$filter .= " OR \"CommonTitle\"='". Convert::raw2sql($arg) ."'";
		}
		return self::get_area($filter,'TkiGeoCountry');
	}
	
	public static function get_subdivision_id($subdivision,$country=null) {
		$res = self::get_subdivision($subdivision,$country);
		return ($res && $res->exists()) ? (int) $res->ID : null;
	}
	
	public static function get_subdivision($subdivision,$country=null) {
		if(empty($subdivision)) return null;
			// By code
		if(preg_match('/^[a-z]{2}-[a-z]{1,3}$/i',$subdivision) === 1) {
			$field = 'Code';
		} elseif(preg_match('/^[a-z]{1,3}$/i',$subdivision) === 1 && !empty($country)) {
			$field = 'AbbrCode';
			$filter = "\"AbbrCode\"='". Convert::raw2sql($subdivision) ."'";
		} else {
			$field = 'Title';
			$filter = "\"Title\"='". Convert::raw2sql($subdivision) ."'";
			$filter .= " OR \"CommonTitle\"='". Convert::raw2sql($subdivision) ."'";
		}
			// Country required without distinct ISO code
		if($field !== 'Code') {
			if(empty($country)) return null;
				// Using ID
			if(is_int($country)) {
				$filter .= " AND \"CountryID\"='". Convert::raw2sql($country) ."'";
			} else {
					// Using code
				if(strlen($country) === 2) {
					$cCode = $country;
				} else {
					// Using title
					$countryObj = self::get_country($country);
					if(!$countryObj) return null;
					$cCode = $countryObj->Code;
				}
				$filter .= " AND \"CountryCode\"='". Convert::raw2sql($cCode) ."'";
			}
		}
		return self::get_area($filter,'TkiGeoSubdivision');
	}
	
	public static function get_locality_id($name,$country,$subdivision=null,$extraFilter=null) {
		$res = self::get_locality($name,$country,$subdivision,$extraFilter);
		return ($res && $res->exists()) ? (int) $res->ID : null;
	}
	
	public static function get_locality($name,$country,$subdivision=null,$extraFilter=null) {
		if(empty($name) || empty($country)) return null;
			// Search by locality title
		$filter = "\"Title\"='". Convert::raw2sql($name) ."'";
		$filter .= " OR \"CommonTitle\"='". Convert::raw2sql($name) ."'";
			// Add country id
		$countryId = self::get_country_id($country);
		if(empty($countryId)) return null;
		$filter .= " AND \"CountryID\"=". $countryId;
			// Add subdivision id
		if(!empty($subdivision)) {
			$subdivisionId = self::get_subdivision_id($subdivision);
			if($subdivisionId) $filter .= " AND \"SubdivisionID\"=". $subdivisionId;
		}
		if(!empty($extraFilter)) $filter .= $extraFilter;
		return self::get_area($filter,'TkiGeoLocality');
	}
}



