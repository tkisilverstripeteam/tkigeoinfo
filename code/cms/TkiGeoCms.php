<?php

class TkiGeoCms extends ModelAdmin {

	static $url_segment = 'geo';

	static $menu_title = 'Geo Data';

	static $menu_priority = 0;

	public static $managed_models = array(
		'TkiGeoCountry',
		'TkiGeoSubdivision',
		'TkiGeoLocality'
	);
	
	public static $model_importers = array(
		'TkiGeoCountry' => 'CsvBulkLoader',
		'TkiGeoSubdivision' => 'CsvBulkLoader'
	);
	
	public static $collection_controller_class = 'TkiGeoCms_CollectionController';

	public static $record_controller_class = 'TkiGeoCms_RecordController';


	function init() {
		$this->showImportForm = array('TkiGeoCountry','TkiGeoSubdivision');
		
		parent::init();
	}

	public function SearchClassSelector() {
		return 'dropdown';
	}
}

class TkiGeoCms_CollectionController extends ModelAdmin_CollectionController {

}

class TkiGeoCms_RecordController extends ModelAdmin_RecordController {

	public function init() {
		parent::init();
		
	}
}


?>
