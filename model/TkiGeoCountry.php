<?php

class TkiGeoCountry extends TkiGeoArea {
	/* ---- Static variables ---- */
	public static $db = array(
		'ExtCode' => 'Varchar(6)'
	);
	public static $has_one = array(
		'Capital' => 'TkiGeoLocality',
		'Continent' => 'TkiGeoContinent'
	);
	public static $has_many = array(
		'Localities' => 'TkiGeoLocality',
		'Subdivisions' => 'TkiGeoSubdivision'
	);
	public static $many_many = array();
	public static $belongs_many_many = array();

	public static $summary_fields = array('Title','Code');
	
	public static $singular_name = 'Country';
	public static $plural_name = 'Countries';
	
	/* ---- Instance variables ---- */

	/* ---- Static methods ---- */

	/* ---- Instance methods ---- */

}

?>
