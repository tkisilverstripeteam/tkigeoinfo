<?php

class TkiGeoSubdivision extends TkiGeoArea {
	/* ---- Static variables ---- */
	public static $db = array(
		'AbbrCode' => 'Varchar(3)',
		'CountryCode' => 'Varchar(2)',
		'SubdivisionType' => "Enum('administration,administrative territory,autonomous city,autonomous community,autonomous district,autonomous region,autonomous republic,autonomous territorial unit,borough,canton,capital,capital city,capital district,capital metropolitan city,city,city corporation,city of county right,commune,constitutional province,council area,county,department,dependency,development region,district,division,economic prefecture,emirate,entity,federal capital territory,federal land,federal territory,federated state,geographical region,geographical unit,governorate,group of islands,indigenous region,island,local council,metropolitan city,metropolitan district,metropolitan region,municipality,outer island,overseas region/department,Pakistan administered area,parish,popularate,prefecture,province,quarter,region,regional council,republic,republican city,special administrative region,special city,special island authority,special municipality,special zone,state,territory,union territory,unitary authority,urban community')"
	);
	public static $has_one = array(
		'Capital' => 'TkiGeoLocality',
		'Country' => 'TkiGeoCountry'
	);
	public static $has_many = array(
		'Localities' => 'TkiGeoLocality'
	);
	public static $many_many = array();
	public static $belongs_many_many = array();
	
	public static $summary_fields = array('Title','Code','CountryCode');
	public static $default_sort = "Code ASC";
	
	public static $singular_name = 'Subdivision';
	public static $plural_name = 'Subdivisions';
	
	/* ---- Instance variables ---- */

	/* ---- Static methods ---- */

	/* ---- Instance methods ---- */

}

?>
