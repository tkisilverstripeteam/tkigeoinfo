<?php

class TkiGeoLocality extends TkiGeoArea {
	/* ---- Static variables ---- */
	public static $db = array(
		'SubdivisionCode' => 'Varchar(6)',
		'CountryCode' => 'Varchar(2)',
		'LocalityType' => "Enum('city,town','city')",
		'Port' => 'Boolean',
		'Latitude' => 'Decimal(9,3)',
		'Longitude' => 'Decimal(9,3)'
	);
	public static $has_one = array(
		'Subdivision' => 'TkiGeoSubdivision',
		'Country' => 'TkiGeoCountry'
	);
	public static $has_many = array();
	public static $many_many = array();
	public static $belongs_many_many = array();
	
	public static $summary_fields = array(
		'Title' => 'Title',
		'Subdivision.Title' => 'Subdivision',
		'Country.Title' => 'Country'
	);
	
	public static $singular_name = 'Locality';
	public static $plural_name = 'Localities';
	
	/* ---- Instance variables ---- */

	/* ---- Static methods ---- */

	/* ---- Instance methods ---- */

}

?>
