<?php
/* Abstract base class for geographical areas
 * @package tkigeoinfo
 */
class TkiGeoArea extends DataObject {
	/* ---- Static variables ---- */
	public static $db = array(
		'Title' => 'Varchar(64)',
		'CommonTitle' => 'Varchar(64)',
		'Code' => 'Varchar(6)',
		'TotalArea' => 'Int'
	);
	public static $has_one = array();
	public static $has_many = array();
	public static $many_many = array();
	public static $belongs_many_many = array();

	public static $extensions = array("Hierarchy");
	
	public static $default_sort = 'Title ASC';
	
	/* ---- Instance variables ---- */

	/* ---- Static methods ---- */

	/* ---- Instance methods ---- */

}

?>
