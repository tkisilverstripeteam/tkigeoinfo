<?php

class TkiGeoHemisphere extends TkiGeoArea {
	/* ---- Static variables ---- */
	public static $db = array();
	public static $has_one = array();
	public static $has_many = array();
	public static $many_many = array();
	public static $belongs_many_many = array();

	public static $summary_fields = array('Title');
	
	public static $singular_name = 'Hemisphere';
	public static $plural_name = 'Hemispheres';
	
	/* ---- Instance variables ---- */

	/* ---- Static methods ---- */

	/* ---- Instance methods ---- */

}

?>
