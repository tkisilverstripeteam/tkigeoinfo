# README #

This module provides geographical data structures, such as continents, countries, and subdivisions. Useful for address and location dropdowns.
Built for SilverStripe 2.4.x.